\documentclass[article, nojss]{jss}

%\VignetteIndexEntry{bsm}
%\VignetteEngine{knitr::knitr}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% declarations for jss.cls %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% almost as usual
\author{Brett Klamer}
\title{\pkg{bsm}: An \proglang{R} Package for Bayesian Survival Modeling}

%% for pretty printing and a nice hypersummary also set:
\Plainauthor{Brett Klamer} %% comma-separated
\Plaintitle{bsm: An R Package for Bayesian Survival Modeling} %% without formatting
%\Shorttitle{} %% a short title (if necessary)

%% an abstract and keywords
\Abstract{
  The \pkg{bsm} package is designed to fit Bayesian Cox survival models with right-censored or interval-censored data, random effects, and time-dependent covariates using either of the \proglang{Stan} or \proglang{JAGS} programming languages. The package allows user chosen prior distributions and model comparison through DIC. The statistical methodology underlying the \pkg{bsm} package is discussed and the package functions are demonstrated with three examples.
}
\Keywords{survival analysis, Bayesian, cox proportional hazards model, \proglang{R}, \proglang{Stan}, \proglang{JAGS}}
\Plainkeywords{survival analysis, bayesian, cox proportional hazards model, R, Stan, JAGS} %% without formatting
%% at least one keyword must be supplied

%% publication information
%% NOTE: Typically, this can be left commented and will be filled out by the technical editor
%% \Volume{50}
%% \Issue{9}
%% \Month{June}
%% \Year{2012}
%% \Submitdate{2012-06-04}
%% \Acceptdate{2012-06-04}

%% The address of (at least) one author should be given
%% in the following format:
\Address{
  Brett Klamer\\
  E-mail: \email{bsm@brettklamer.com}\\
  URL: \url{http://brettklamer.com}
}
%% It is also possible to add a telephone and fax number
%% before the e-mail in the following format:
%% Telephone: +43/512/507-7103
%% Fax: +43/512/507-2851

%% for those who use Sweave please include the following line (with % symbols):
%% need no \usepackage{Sweave.sty}

%% end of declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[final]{microtype}
\usepackage{amsmath}
\usepackage{float}

\begin{document}

% knitr options
<<setup, include=FALSE, cache=FALSE>>=
library(knitr)

# inline hook to process the output of Sexpr{} statements to just 2 digits
inline_hook <- function(x) {
  if(is.numeric(x)) x <- round(x, 2)
  paste(as.character(x), collapse=", ")
}
knit_hooks$set(inline = inline_hook)

# cache all chunks
opts_chunk$set(cache = TRUE)

# no dingbats in pdf graphs
pdf.options(useDingbats = FALSE) # unnecessary with tikz, but a good idea...

#==============================================================================
## Embed latex font in R graphics
# Use tikz, but have Cairo as a backup.
# when using tikz, make sure pgf and preview packages are installed in latex.
# DO NOT load those packages in the preamble
#==============================================================================
#options(tikzMetricsDictionary = "/path/to/dictionary/location")
#library(tikzDevice)
#opts_chunk$set(dev = 'tikz')
#opts_chunk$set(dev.args=list(pointsize=10))

library(Cairo)
myfont <- "Garamond"
CairoFonts(regular = paste(myfont, "style = Regular", sep = ":"),
           bold = paste(myfont, "style = Bold", sep = ":"),
           italic = paste(myfont, "style = Italic", sep = ":"),
           bolditalic = paste(myfont, "style = Bold Italic, BoldItalic", sep = ":"))
pdf <- CairoPDF
opts_chunk$set(dev = 'CairoPDF')
opts_chunk$set(dev.args=list(pointsize=10))
@

\section[Introduction]{Introduction}

Survival analysis is a statistical method whose outcome of interest is the length of time to the occurrence of an event. The event of interest may be a death, contraction of disease, recovery, or any other change in condition for the subject. This broad definition has made survival analysis a popular technique in many fields such as medicine, social sciences, engineering, and ecology. Survival analysis uses a special class of regression models that are designed to handle subject censoring and time-dependent covariates \citep{allison2010}. The most commonly used regression model is the Cox \citep{cox1972} Proportional Hazards (PH) regression model \citep{therneau2000}.

Censoring occurs when the final time to an event is not observed for a particular subject \citep{collett2003}. There are three common forms of censoring: right, left, and interval. Right censoring occurs when the subject does not experience the event during the study time. Left censoring come to pass when the subject is measured once and the event has already occurred. Interval censoring occurs when an event takes place between two known time points, but the exact event time is unknown. Interval censoring is common in studies where periodic follow-ups take place while right censoring occurs under continuous measurement \citep{kleinbaum2011}. 

There are two different types of time-dependent covariates: internal and external. Internal covariates are generated by the subject itself and are only measured given the subjects existence (e.g. blood pressure). External covariates are measured independent of the subject and usually apply to all subjects (e.g. environmental conditions). Survival models are most appropriate for external time-dependent covariates \citep{rizopoulos2012}. Time-dependent covariates must also be chosen with care. Subject age, for example, is a predictable future process. If age is assumed to have a linear effect on risk, then it will automatically be represented in the baseline hazard. However, if the effect of age is non-linear, then it should be included as a time-dependent covariate \citep{hosmer2008, time-dep}. If you have internal time-dependent covariates, consider using a technique known as ``joint modeling''.  This method creates a joint model usually based on a longitudinal linear mixed-effects model and a Cox survival model. Packages available in \proglang{R} include the frequentist \pkg{JM} package \citep{jm} and the Bayesian \pkg{JMbayes} package \citep{jmbayes} among others. 

It's also common to for survival models to be constructed with time-dependent regression coefficients. These models are helpful when the proportional hazards assumption of the standard Cox model is violated. However, it turns out the time-dependent coefficient model is equal to the time-dependent covariate model under the same function of time \citep{thomas2014}. Therefore fitting time-dependent covariates or an interaction between time and time-fixed covariates is sufficient for this case.

The \pkg{bsm} package is designed to provide a user friendly way for researchers to fit Bayesian Cox survival models. This is achieved by using syntax similar to that of the eminent \pkg{survival} package \citep{survival-package} and creating a Bayesian framework without the need for learning a new software language and writing (and debugging) a particular model. \pkg{bsm} integrates with either of the \proglang{JAGS} \citep{jags} or \proglang{Stan} \citep{stan-software} software through the respective \proglang{R} packages \pkg{runjags} \citep{runjags} or \pkg{RStan} \citep{rstan-software}. Bayesian inference is accomplished through randomly drawing samples from the posterior using Markov Chain Monte Carlo (MCMC) algorithms. The \proglang{JAGS} language uses Gibbs sampling \citep{geman, gelfand1990} while \proglang{Stan} uses Hamiltonian Monte Carlo (HMC) \citep{hmc} and its extension, the No-U-Turn sampler (NUTS) \citep{nuts}. HMC has advantages over Gibbs sampling with high-dimensional models and non-conjugate priors \citep{nuts}.

\section[Model Description]{Model Description}

The Cox PH model is modeled through the hazard function as follows:
\begin{equation}
\label{cox-ph-model}
\begin{aligned}
h_i(t;\mathbf{X}(t)) &= h_0(t)e^{\sum_{j=1}^p(\beta_{j}X_{ij}(t))}
\end{aligned}
\end{equation}
where $h_i(t;\mathbf{X}(t))$ is the hazard value at time $t$ for subject $i$, $h_0(t)$ is the baseline hazard function at time $t$, $p$ is the number of covariates, $\beta_j$ is the $j$th unknown regression coefficient ($\beta$ being the natural log of the hazard ratio), and $X_{ij}(t)$ is the $j$th possibly time-dependent covariate for subject $i$. Note that the baseline hazard function describes the unknown and unspecified hazard for all subjects given that the covariates are not considered. Estimation of the $\beta$ coefficients take place through Cox's innovative partial likelihood \citep{cox1972, cox1975}.

Although the traditional Cox PH model can only fit right-censored data, various extensions of the Cox model have been proposed for interval-censored data. Several R packages fit these type of models, for instance, the frequentist \pkg{intcox} package by \cite{intcox} utilizes the iterative convex minorant algorithm introduced by \cite{groeneboom1992}, and the Bayesian \pkg{dynsurv} package by \cite{dynsurv} utilizes a dynamic Bayesian Cox model \citep{wang2013}. An alternative method for fitting interval-censored data is by using event time imputation. Right-point imputation treats the right point of the time interval as the true event time and mid-point imputation treats the average of the left and right points of the time interval as the true event time. This transforms the data to be right-censored and amenable for the traditional Cox PH model. Mid-point imputation generally performs slightly better than right-point imputation and compares favorably to more sophisticated methods \citep{chen2012, lee2011}. Imputation methods become more reasonable as the number of periodic follow-ups increases and the interval censoring is contained within a short time span \citep{chen2012}.

\subsection{Counting Process Model}

A common generalization of the Cox model is through a counting process formulation. Counting processes are based on martingale theory and were introduced to survival analysis through the work of \cite{aalen1975, aalen1978}. Counting processes utilize the fact that a models local behavior (in time) is a Poisson process. That is, the number of events in a given interval of time has a Poisson distribution \citep{aalen2008}. This work was further illustrated by \cite{andersen1982}, creating an accepted notation for counting processes with the Cox model \citep{andersen1993}. Refer to \cite{gill1984} or \cite{hosmer2008} for further review of the counting process approach to frequentist survival analysis.

The Andersen-Gill (AG) model is modeled through the intensity process as follows:
\begin{equation}
\label{ag-model}
\begin{aligned}
\lambda_i(t;\mathbf{X}(t)) &= Y_i(t) h_0(t)e^{\sum_{j=1}^p(\beta_{j}X_{ij}(t))}
\end{aligned}
\end{equation}
where $\lambda_i(t;\mathbf{X}(t))$ is the intensity process for a counting process at time $t$ for subject $i$ and $Y_i(t)$ is an indicator process taking the value 0 or 1 according to whether or not the $i$th subject is still under observation at time $t$. The frequentist AG model provides a suitable approach for practical implementation of time-dependent covariates, time-dependent strata, left truncation (not left censoring), and multiple events per subject \citep{therneau2000}. The \pkg{survival} package implements the AG model for the previously mentioned data types \citep{survival-package}.

There are a variety of approaches for Bayesian Cox modeling, however, the gamma process prior model described by \cite{kalbfleisch1978} is perhaps the most used \citep{ibrahim2001}. \cite{clayton1991, clayton1994} defined this model using the counting process formulation of \cite{andersen1982} under a Gibbs sampling framework. Based on \cite{clayton1994}, \cite{bugs-examples} wrote example code for a Bayesian Cox survival analysis using the \proglang{WinBugs} \citep{bugs-software} programming language. They analyzed the classic \cite{gehan2011} leukemia patient dataset and even accounted for its matched pair design using a frailty model. The \pkg{bsm} package uses the Bayesian Cox model formulation from \cite{bugs-examples} and extends it to allow time-dependent covariates. An alternative data representation of the gamma process model can be found in \cite{christensen2011}. 

\subsection{Data Description}

If the dataset contains time-dependent covariates, use the counting process input style employed by the \pkg{survival} package. It's also important to be consistent in the naming of dataframe variables. I recommend $id$ as a subject identifier, $start$ and $stop$ to indicate the beginning and end of a particular time interval, $event=1$ if an event has occurred and $event=0$ if the observation is censored, and any short, meaningful, lowercase name for predictor variables. 
<<time-dep-data, echo = FALSE>>=
# A simple dataframe example
library(xtable)
set.seed(0488)
id <- as.integer(c(1, 1, 2, 2, 2, 3, 3, 3, 3))
start <- as.integer(c(0, 1, 0, 1, 2, 0, 1, 2, 3))
stop <- as.integer(c(1, 2, 1, 2, 3, 1, 2, 3, 4))
event <- as.integer(c(0, 1, 0, 0, 1, 0, 0, 0, 0))
covariate <- rnorm(9)
df <- data.frame(id, start, stop, event, covariate)
@
<<time-dep-data-print, results = 'asis', echo = FALSE>>=
print(xtable(df, caption='An example dataframe for time-dependent covariates', label='table:example-data'), caption.placement = "top", include.rownames=FALSE)
@

\pkg{bsm} will then transform the dataframe with the following process: \\
Let $N$ be the number of unique subjects.
\begin{enumerate}
\item The unique event times, represented by $t$ with length $T$, and the largest censored value (provided the largest censor value is larger than all unique death times) will be ordered into a vector of length $T+1$.
\item A guess at the unknown hazard function, $\mathop{\textnormal{d}H^*_0(t)}=r\mathop{\textnormal{d}t}$, where $r$ is a guess at the failure rate and $\mathop{\textnormal{d}t} = t_{i+1}-t_i$ for $i = 1,...,T$ are place into a vector of length $T \times 1$. This is thought of as prior information for the unknown hazard function, but we will calculate it using the data \citep{bugs-examples}.
\item The left-continuous at-risk process, represented by $Y_i(t)$, is transformed into an $N \times T$ matrix.
\[
    Y_i(t)= 
\begin{cases}
    1, & \text{if subject } i \text{ is still observed in } (t,t+\textnormal{d}t]\\
    0, & \text{otherwise}
\end{cases}
\]
\item The right-continuous counting process for the number of events up to unique event time $t$, represented by $K_i(t)$, is transformed into an $N \times T$ matrix.
\[
    K_i(t)= 
\begin{cases}
    1, & \text{if subject } i \text{ experiences an event in } [t,t+\textnormal{d}t)\\
    0, & \text{otherwise}
\end{cases}
\]
\item Possibly time-dependent covariates, represented by $\mathbf{x}_i(t)$, are transformed into matrices of size $N \times T$. It is assumed covariate values are constant in the left-continuous, predictable process in $(t,t+\textnormal{d}t]$.
\end{enumerate}

As indicated in \cite{kalbfleisch1973}, the partition of the time axis should not depend on the data. However, because it is best if the event times are equally spread across the partitions, we will ignore the fact that construction was based on event times \citep{christensen2011}. Unlike traditional Cox PH models, the baseline hazard is not left unspecified. This is a requirement of Bayesian ideology and the \proglang{JAGS} and \proglang{Stan} languages. Instead, the data model for the integrated baseline hazard will be defined as a step function over the unique event times. Note that if the number of parameters, $p+T$, is greater than number of unique subjects then the survival and baseline hazard are non-identifiable \citep{christensen2011}.

\subsection{Parameter Description}

Based on \cite{bugs-examples}, we use the following posterior:
\begin{equation}
\label{posterior}
\begin{aligned}
p(\beta, H_0(\cdot) | D) \propto L(\beta,H_0(\cdot)|D)p(\beta)p(H_0(\cdot))
\end{aligned}
\end{equation}
where the observed data is $D = \{K_i(t), Y_i(t), \mathbf{x}_i(t); i = 1,\ldots,n\}$, and the unknown parameters $\beta$ and $H_0(t) = \int_0^th_0(u)\mathop{\textnormal{d}u}$. The likelihood is found to be
\begin{equation}
\label{propto-likelihood}
\begin{aligned}
L(\beta,H_0(\cdot)|D) &\propto \prod_{i=1}^n \left[ \prod_{t\geq0} \left( Y_i(t)e^{\left( \beta x_i(t) \right)} h_0(t) \right)^{\textnormal{d}K_i(t)}  \right] e^{-\int_{t\geq0}Y_i(t)e^{\left( \beta x_i(t) \right)} h_0(t) \mathop{\textnormal{d}(t)}} \\
                      &\overset{ind}{\sim} \textnormal{Poisson}\left( Y_i(t)e^{\left( \beta x_i(t) \right)} \mathop{\textnormal{d}H_0(t)} \right)
\end{aligned}
\end{equation}
where $\mathop{\textnormal{d}H_0(t)}$ is the increment in the integrated baseline hazard function during a particular time interval. This likelihood assumes that time is continuous and no two events can occur at the same time. 

Suppose there are two subjects with the same event time. We do not know whether or not to consider subject one still at-risk while subject two experiences the event, or vice versa. Frequentist methods often employ \cite{breslow1974} or \cite{efron1977} approximations to correct for ties. However, there is no best method to solve this in a Bayesian framework using the JAGS or Stan language as indicated by \cite{bugs-examples}. A naive method, suggested by \cite{kalbfleisch1978}, randomly perturbs the tied event times and continues the analysis as normal. This method may be particularly useful under interval censoring as the event times are only imputed, resulting in no loss of information. However, this method is not implemented in \pkg{bsm}. Although deviation from this assumption should not be neglected, the current method without tie correction should still provide reliable estimates from data with a minimal amount of ties.

The prior for the integrated baseline hazard is given as
\begin{equation}
\label{hazard-prior}
\begin{aligned}
p(H_0(\cdot)) &\overset{ind}{\sim} \textnormal{Gamma}\left( c \mathop{\textnormal{d}H_0^*(t)} , c\right)
\end{aligned}
\end{equation}
where $c$ is degree of confidence in the hazard guess. Small values of $c$ relate to weak belief while large values relate to a stronger belief. A discussion of choice of $c$ can be found in \cite{sinha2011}. The prior for $\beta$ is defined as
\begin{equation}
\label{beta-prior}
\begin{aligned}
p(\beta) \overset{ind}{\sim} \textnormal{N}\left( \mu, \tau \right)
\end{aligned}
\end{equation}

Users of \pkg{bsm} have the ability to specify the parameters of $p(\beta)$ and $p(H_0(\cdot))$ or use the default weakly informative priors.

\subsection{Random Effects Model}

The random effects survival model, commonly known as a frailty model in survival literature, contains a variable that describes the risk for subjects or groups of subjects \citep{therneau2000}. The Bayesian gamma process survival model with a random effect is defined as \citep{bugs-examples}
\begin{equation}
\label{random-effect-model}
\begin{aligned}
\lambda_i(t;\mathbf{X}(t)) &= Y_i(t) h_0(t)e^{\sum_{j=1}^p(\beta_{j}X_{ij}(t) + b_i)}
\end{aligned}
\end{equation}
where the the prior distribution for random effect $b$ is
\begin{equation}
\label{random-effect-prior}
\begin{aligned}
p(b_i) \overset{ind}{\sim} \textnormal{N}\left( \mu, \tau \right)
\end{aligned}
\end{equation}
where $\tau \sim \textnormal{Gamma}(\textnormal{shape}, \textnormal{rate})$ and weakly informative choices for both the shape and rate are 0.001. This model is chosen to produce log-concave conditional distributions for easier implementation in software \citep{bugs-examples, duchateau2010}.

\subsection{Model Selection}

When using \proglang{JAGS} as the back-end for MCMC sampling in \pkg{bsm}, the deviance information criterion (DIC) \citep{spiegelhalter2002} is offered for model selection. DIC offers a Bayesian version of information criteria similar to that of the Akaike Information Crierion (AIC) \citep{akaike1973} where it represents a form of cross-validation and predictive measure. Following \cite{gelman2013}, DIC is defined as 
\begin{equation}
\label{dic}
\begin{aligned}
\textnormal{DIC} &= -2\log p(y|\hat{\theta}_{\textnormal{Bayes}}) + 2\left(p_{\textnormal{DIC}}\right) \\
&= \textnormal{goodness of fit} + \textnormal{complexity}
\end{aligned}
\end{equation}
where $y$ is the data, $\hat{\theta}$ is the maximum likelihood estimate, $\hat{\theta}_{\textnormal{Bayes}} = E(\theta|y)$ is the posterior mean, and $p_{\textnormal{DIC}}$ is the effective number of parameters, defined as
\begin{equation}
\label{pd}
\begin{aligned}
p_{\textnormal{DIC}} &= 2 \left( \log p(y|\hat{\theta}_{\textnormal{Bayes}}) - E_{\textnormal{posterior}}(\log p(y|\theta)) \right).
\end{aligned}
\end{equation}
Smaller values of DIC indicate better measures of fit when comparing models, but there is no scale for the difference in DIC between two models \cite{plummer2008}. Some have suggested that differences of 5 or more indicate optimum models \citep{mrcdic}. The specific formulation of DIC as used by \proglang{JAGS} can be found in \cite{plummer2008}. 

Unfortunately, DIC may not be the best model selection tool for Cox-type models as evidenced by \cite{carlin2008}:
\begin{quote}
As of the present writing, the use of $p_{\textnormal{DIC}}$ and DIC remains common, though with a general caveat that they should be avoided for models lying far outside the exponential family, where it is not at all clear that a normal approximation to the posterior is sensible. \ldots 

Besides being rather ad hoc, a problem with penalized likelihood approaches is that the usual choices of penalty function are motivated by asymptotic arguments that are sometimes hard to justify in practice. While $p_{\textnormal{DIC}}$ does provide one definition of the ``effective size'' of hierarchical models, it is difficult to justify in many nonparametric or other highly parametrized settings (e.g., Cox-type survival models).
\end{quote}
However, in practice, DIC and LPML tend to provide similar results \citep{luping2008}.

The Logarithm of the Pseudomarginal Likelihood (LPML) \citep{geisser1979} is known to be computationally stable \citep{carlin2008} and well accepted with model selection in survival analysis \citep{ibrahim2001}. The LPML is calculated using the Conditional Predictive Ordinate (CPO) \citep{gelman2013}
\begin{equation}
\label{cpo}
\begin{aligned}
\textnormal{CPO}_i = p(y_i | y_{-i})
\end{aligned}
\end{equation}
where $y$ is the data and $y_{-i}$ denotes the data with the $i$th case deleted, resulting in \citep{ibrahim2001}
\begin{equation}
\label{lpml}
\begin{aligned}
\textnormal{LPML} = \sum_{i=1}^n \log (\textnormal{CPO}_i).
\end{aligned}
\end{equation}
LPML provides a leave-one-out cross-validatory approach where larger values indicate better measures of fit when comparing models \citep{carlin2008}. Currently, LPML is not implemented in \pkg{bsm}.

\section[Examples]{Examples}

To illustrate \pkg{bsm}'s syntax and capabilities, we will consider three different examples. If you wish to install the development version of bsm, run the following function from the \pkg{devtools} \citep{devtools} package:
<<install-bsm, eval = FALSE>>=
devtools::install_bitbucket("bklamer/bsm")
@
\pkg{bsm} contains full documentation for functions and their arguments. If you use \proglang{Stan} as the back-end for MCMC sampling, you will also need to have a \proglang{C++} compiler. This is generally provided for Windows users with the \pkg{Rtools} program (available at \url{https://cran.r-project.org/bin/windows/Rtools/}).

\subsection{Leukemia}

The Gehan-Frierich leukemia dataset \citep{gehan2011} is commonly used as an example in survival analysis. The dataset is contained within the \pkg{bsm} package under the name \texttt{leukemia}. Table \ref{table:leukemia} contains the first few observations. The covariates in this dataset are time-fixed. 

Consider a survival model of the form
\begin{equation}
\label{leukemia-model}
\begin{aligned}
\lambda_i(t;\textnormal{treatment}) &= Y_i(t) h_0(t)e^{(\beta (\textnormal{treatment}_{i}))}.
\end{aligned}
\end{equation}
This can be fit using the following code:
<<leukemia-data, echo = FALSE, message = FALSE, warning = FALSE>>=
library(xtable)
library(stargazer)
library(ggmcmc)
library(dplyr)
library(bsm)
df <- leukemia
@
<<leukemia-data-print, results = 'asis', echo = FALSE>>=
print(xtable(head(df), caption='The Gehan-Frierich leukemia dataset', label='table:leukemia'), caption.placement = "top", include.rownames=FALSE)
@
<<fit-leukemia-no, eval = FALSE, tidy = TRUE>>=
# Leukemia data model
leukemia_model <- bsm(Surv(time, event) ~ treatment, data = leukemia)
@
<<fit-leukemia-yes, echo = FALSE, message = FALSE, results = 'hide'>>=
leukemia_model <- bsm(Surv(time, event) ~ treatment, data = leukemia)
@
<<fit-leukemia-print, results = 'asis', echo = FALSE>>=
# print(xtable(summary(leukemia_model), caption='Posterior estimate of $\\beta$', label='table:fit-leukemia'), caption.placement = "top", include.rownames=FALSE)
leukemia_median <- leukemia_model$summary$quantiles[3]
leukemia_hazard <- exp(leukemia_median)
leukemia_sd <- leukemia_model$summary$statistics[2]
nchain <- leukemia_model$summary$nchain
sample <- leukemia_model$sample
warmup <- leukemia_model$burnin
@
Results are from $\Sexpr{nchain}$ chains each taking $\Sexpr{sample}$ samples from the posterior after a $\Sexpr{warmup}$ sample warmup. The posterior median $\beta_{\textnormal{treatment}} = \Sexpr{leukemia_median}$ and standard deviation of $\Sexpr{leukemia_sd}$. This compares favorably with posterior estimates from others \citep{bugs-examples}. The hazard ratio $e^{\beta_{\textnormal{treatment}}} = e^{\Sexpr{leukemia_median}} = \Sexpr{leukemia_hazard}$ indicates that those in the control (placebo) group are $\Sexpr{leukemia_hazard}$ times as likely to have a relapse of leukemia.

Since we used weakly informative priors, we would expect similar results from the frequentist approach which are contained in Table \ref{leukemia-model-freq}.
<<fit-leukemia-freq-no, eval = FALSE, tidy = TRUE>>=
# Leukemia data model using coxph() from survival package
leukemia_model_freq <- coxph(Surv(time, event) ~ treatment, data = leukemia)
summary(leukemia_model_freq)
@
<<fit-leukemia-freq-yes, echo = FALSE, message = FALSE, results = 'hide'>>=
leukemia_model_freq <- coxph(Surv(time, event) ~ treatment, data = leukemia)
@
<<fit-leukemia-freq-print, results = 'asis', echo = FALSE>>=
stargazer(leukemia_model_freq, title="Leukemia model fit from frequentist survival package.", omit.stat = c("max.rsq","logrank"), omit.table.layout = "n", label = "leukemia-model-freq")
@
The posterior and trace plots of $\beta_{\textnormal{treatment}}$ can be found in Figure \ref{fig:fit-leukemia-density} and Figure \ref{fig:fit-leukemia-trace} respectively.
<<fit-leukemia-diagnostic-plots, eval = FALSE>>=
# from the ggmcmc package
ggs_density(ggs(leukemia_model$mcmc))
ggs_traceplot(ggs(leukemia_model$mcmc))
@
<<fit-leukemia-density, echo = FALSE, fig.cap = 'Posterior plot of $\\beta_{\\textnormal{treatment}}$.', fig.lp = 'fig:', fig.width = 5, fig.height = 2.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_density(ggs(leukemia_model$mcmc))
@
<<fit-leukemia-trace, echo = FALSE, fig.cap = 'Trace plot of $\\beta_{\\textnormal{treatment}}$.', fig.lp = 'fig:', fig.width = 5, fig.height = 2.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_traceplot(ggs(leukemia_model$mcmc))
@
\subsection{Leukemia with Random Effect}
The leukemia data was actually a paired design where one subject from each pair received the active ingredient. This can be modeled using a group-specific random effect with the following model \citep{bugs-examples}
\begin{equation}
\label{leukemia-frailty-model}
\begin{aligned}
\lambda_i(t;\textnormal{treatment}, \textnormal{pair}) &= Y_i(t) h_0(t)e^{(\beta (\textnormal{treatment}_{i}) + b_{pair_{i}})}.
\end{aligned}
\end{equation}
<<fit-leukemia-frailty-no, eval = FALSE, tidy = TRUE>>=
# Leukemia random effect data model
leukemia_frailty_model <- bsm(Surv(time, event) ~ treatment + frailty(pair), data = leukemia)
@
<<fit-leukemia-frailty-yes, echo = FALSE, message = FALSE, results = 'hide'>>=
leukemia_frailty_model <- bsm(Surv(time, event) ~ treatment + frailty(pair), data = leukemia)
@
<<fit-leukemia-frailty-print, results = 'asis', echo = FALSE>>=
# print(xtable(summary(leukemia_frailty_model), caption='Posterior estimate of $\\beta$', label='table:fit-leukemia'), caption.placement = "top", include.rownames=FALSE)
leukemia_trt_median <- leukemia_frailty_model$summary$quantiles[1,3]
leukemia_trt_hazard <- exp(leukemia_trt_median)
leukemia_trt_sd <- leukemia_frailty_model$summary$statistics[1,2]
sigma <- leukemia_frailty_model$summary$statistics[2,1]
nchain <- leukemia_frailty_model$summary$nchain
sample <- leukemia_frailty_model$sample
warmup <- leukemia_frailty_model$burnin
@
Results are from $\Sexpr{nchain}$ chains each taking $\Sexpr{sample}$ samples from the posterior after a $\Sexpr{warmup}$ sample warmup. The posterior median $\beta_{\textnormal{treatment}} = \Sexpr{leukemia_trt_median}$ is slightly larger than the model without a random effect. The posterior mean of $\sqrt{\frac{1}{\tau}} = \sigma = \Sexpr{sigma}$ was small, suggesting that there is little association between event times within the matched pairs.

<<fit-leukemia-frailty-density, echo = FALSE, fig.cap = 'Posterior plot of $\\beta_{\\textnormal{treatment}}$ and $\\sigma$', fig.lp = 'fig:', fig.width = 5, fig.height = 2.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_density(ggs(leukemia_frailty_model$mcmc))
@
<<fit-leukemia-frailty-trace, echo = FALSE, fig.cap = 'Trace plot of $\\beta_{\\textnormal{treatment}}$ and $\\sigma$.', fig.lp = 'fig:', fig.width = 5, fig.height = 2.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_traceplot(ggs(leukemia_frailty_model$mcmc))
@

\subsection{Heart Transplant}

The Stanford heart transplant dataset \citep{crowley1977} is another commonly used example in survival analysis. The dataset is contained within the \pkg{bsm} package under the name \texttt{heart}. Table \ref{table:heart} contains the first few observations. The transplant covariate in this dataset is time-dependent. 

Consider a survival model of the form
\begin{equation}
\label{heart-model}
\begin{aligned}
\lambda_i(t;\textnormal{transplant}(t)) &= Y_i(t) h_0(t)e^{(\beta (\textnormal{transplant}_{i}(t)))}.
\end{aligned}
\end{equation}
This can be fit using the following code:
<<heart-data, echo = FALSE, message = FALSE>>=
df <- heart
@
<<heart-data-print, results = 'asis', echo = FALSE>>=
print(xtable(head(df), caption='The Stanford heart transplant dataset', label='table:heart'), caption.placement = "top", include.rownames=FALSE)
@
<<fit-heart-no, eval = FALSE, tidy = TRUE>>=
# Heart data model
heart_model <- bsm(Surv(start, stop, event) ~ transplant, id = "id", data = heart)
@
<<fit-heart-yes, echo = FALSE, message = FALSE, results = 'hide', warning = FALSE>>=
heart_model <- bsm(Surv(start, stop, event) ~ transplant, id = "id", data = heart)
@
<<fit-heart-print, results = 'asis', echo = FALSE>>=
# print(xtable(summary(heart_model), caption='Posterior estimate of $\\beta$', label='table:fit-heart'), caption.placement = "top", include.rownames=FALSE)
heart_median <- heart_model$summary$quantiles[3]
heart_hazard <- exp(heart_median)
heart_sd <- heart_model$summary$statistics[2]
nchain <- heart_model$summary$nchain
sample <- heart_model$sample
warmup <- heart_model$burnin
@
Results are from $\Sexpr{nchain}$ chains each taking $\Sexpr{sample}$ samples from the posterior after a $\Sexpr{warmup}$ sample warmup. The posterior median $\beta_{\textnormal{transplant}} = \Sexpr{heart_median}$ and standard deviation of $\Sexpr{heart_sd}$. The hazard ratio $e^{\beta_{\textnormal{transplant}}} = e^{\Sexpr{heart_median}} = \Sexpr{heart_hazard}$ indicates that those who received transplants are $\Sexpr{heart_hazard}$ times as likely to die. Of course, the posterior of $\beta_{\textnormal{transplant}}$ covers 0, so the significance is not that strong. We have also ignored other potentially important covariates from the dataset.

Since we used weakly informative priors, we would expect similar results from the frequentist approach which are contained in Table \ref{heart-model-freq}.
<<fit-heart-freq-no, eval = FALSE, tidy = TRUE>>=
# Heart data model using coxph() from survival package
heart_model_freq <- coxph(Surv(start, stop, event) ~ transplant, data = heart)
summary(heart_model_freq)
@
<<fit-heart-freq-yes, echo = FALSE, message = FALSE, results = 'hide'>>=
heart_model_freq <- coxph(Surv(start, stop, event) ~ transplant, data = heart)
@
<<fit-heart-freq-print, results = 'asis', echo = FALSE>>=
stargazer(heart_model_freq, title="Heart model fit from frequentist survival package.", omit.stat = c("max.rsq","logrank"), omit.table.layout = "n", label = "heart-model-freq")
@
The posterior and trace plots of $\beta_{\textnormal{tranplant}}$ can be found in Figure \ref{fig:fit-heart-density} and Figure \ref{fig:fit-heart-trace} respectively.
<<fit-heart-diagnostic-plots, eval = FALSE>>=
# from the ggmcmc package
ggs_density(ggs(heart_model$mcmc))
ggs_traceplot(ggs(heart_model$mcmc))
@
<<fit-heart-density, echo = FALSE, fig.cap = 'Posterior plot of $\\beta_{\\textnormal{transplant}}$.', fig.lp = 'fig:', fig.width = 5, fig.height = 2.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_density(ggs(heart_model$mcmc))
@
<<fit-heart-trace, echo = FALSE, fig.cap = 'Trace plot of $\\beta_{\\textnormal{transplant}}$.', fig.lp = 'fig:', fig.width = 5, fig.height = 2.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_traceplot(ggs(heart_model$mcmc))
@

\subsection{Sage-Grouse}

Finally, we will consider the female sage-grouse dataset \citep{kirol2015}. The dataset is contained within the \pkg{bsm} package under the name \texttt{female}. Table \ref{table:female} contains the first few observations. All covariates in this dataset are time-dependent. 

Consider a survival model of the form
\begin{equation}
\label{female-model}
\begin{aligned}
\lambda_i(t;X_1(t), X_2(t), X_3(t)) &= Y_i(t) h_0(t)e^{(\beta_1 X_{i,1}(t) + \beta_2 X_{i,2}(t) + \beta_3 X_{i,3}(t))}.
\end{aligned}
\end{equation}
This can be fit using the following code:
<<female-data, echo = FALSE, message = FALSE>>=
df <- female
@
<<female-data-print, results = 'asis', echo = FALSE>>=
print(xtable(head(df), caption='The female sage-grouse dataset', label='table:female'), caption.placement = "top", include.rownames=FALSE)
@
<<fit-female-no, eval = FALSE, tidy = TRUE>>=
# Remove NA cases
missing <- female[is.na(female$shrub_height), ][[1]]
female <- female %>% filter(!(id %in% missing)) %>% droplevels(.)
# Female data model
female_model <- bsm(Surv(start, stop, event) ~ roughness + edge_distance + shrub_height, id = "id", data = female)
@
<<fit-female-yes, echo = FALSE, message = FALSE, results = 'hide', warning = FALSE>>=
# Remove NA cases
missing <- female[is.na(female$shrub_height), ][[1]]
female <- female %>% filter(!(id %in% missing)) %>% droplevels(.)
# Female data model
female_model <- bsm(Surv(start, stop, event) ~ roughness + edge_distance + shrub_height, id = "id", data = female)
@
<<fit-female-print, results = 'asis', echo = FALSE>>=
# print(xtable(summary(female_model), caption='Posterior estimate of $\\beta$', label='table:fit-female'), caption.placement = "top", include.rownames=FALSE)
female_median_roughness <- female_model$summary$quantiles[1,3]
female_median_edge <- female_model$summary$quantiles[2,3]
female_median_shrub <- female_model$summary$quantiles[3,3]
female_hazard_roughness <- exp(female_median_roughness)
female_hazard_edge <- exp(female_median_edge)
female_hazard_shrub <- exp(female_median_shrub)
female_sd_roughness <- female_model$summary$statistics[1,2]
female_sd_edge <- female_model$summary$statistics[2,2]
female_sd_shrub <- female_model$summary$statistics[3,2]
nchain <- female_model$summary$nchain
sample <- female_model$sample
warmup <- female_model$burnin
@
Results are from $\Sexpr{nchain}$ chains each taking $\Sexpr{sample}$ samples from the posterior after a $\Sexpr{warmup}$ sample warmup. The posterior medians are $\beta_{\textnormal{roughness}} = \Sexpr{female_median_roughness}$, $\beta_{\textnormal{edge}} = \Sexpr{female_median_edge}$, and $\beta_{\textnormal{shrub}} = \Sexpr{female_median_shrub}$. The hazard ratios are $e^{\beta_{\textnormal{roughness}}} = \Sexpr{female_hazard_roughness}$, $e^{\beta_{\textnormal{edge}}} = \Sexpr{female_hazard_edge}$, and $e^{\beta_{\textnormal{shrub}}} = \Sexpr{female_hazard_shrub}$. This indicates that adult female birds suffer increased risk as terrain roughness and distance to anthropogenic edges increase, while risk decreases as variability in shrub height increases.

The posterior and trace plots can be found below.
<<fit-female-diagnostic-plots, eval = FALSE>>=
# from the ggmcmc package
ggs_density(ggs(female_model$mcmc))
ggs_traceplot(ggs(female_model$mcmc))
@
<<fit-female-density, echo = FALSE, fig.cap = 'Posterior plots for female sage-grouse model.', fig.lp = 'fig:', fig.width = 5, fig.height = 3.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_density(ggs(female_model$mcmc))
@
<<fit-female-trace, echo = FALSE, fig.cap = 'Trace plots for female sage-grouse model.', fig.lp = 'fig:', fig.width = 5, fig.height = 3.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_traceplot(ggs(female_model$mcmc))
@

\section[Conclusion]{Conclusion}

This paper is meant to provide a general overview of the \pkg{bsm} package and it's implementation of Bayesian survival models. There are many Bayesian survival packages available for \proglang{R}, however most are missing critical components for common survival analyses or are not user-friendly. \pkg{bsm} aims to fill this role by implementing the common Bayesian gamma process Cox model with a familiar \pkg{survival}-like syntax. The survival model in \pkg{bsm} allows time-dependent covariates, right-censored and mid-point imputed interval-censored data, and random effects. \pkg{bsm} is not yet available through CRAN.

\section[Acknowledgments]{Acknowledgments}

Acknowledgments...

\bibliography{bibliography}

\end{document}
