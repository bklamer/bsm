# Does the formula contain a Surv object?
## Returns TRUE or FALSE.
is_surv <- function(formula, data) {
  inherits(model.extract(model.frame(formula, data), "response"), "Surv")
}

# Type of Surv object in the formula.
## Returns character string of attribute type.
surv_type <- function(formula, data) {
  attr(model.extract(model.frame(formula, data), "response"), "type")
}
