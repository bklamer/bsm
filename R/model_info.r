#' Create a list of Bayesian model information
#'
#' Create a list of model information needed to fit a Bayesian survival model.
#'
#' @param formula A formula object, with the response on the left of a \code{~} operator, and the covariate terms on the right. The response must be a survival object as returned by the \code{Surv} function from the survival package.
#' @param data A \code{data.frame} in which to interpret the variables named in the formula and \code{id} argument.
#' @param id A character string of the subject id's column name in the \code{data.frame} (if the \code{data} argument is provided) or a vector of subject id's (if the \code{data} argument is not provided). The \code{id} argument is only needed for time-varying covariate survival data.
#' @param model_selection A character vector of information criteria variables. The reserved variable names for information criteria are \code{"deviance"}, \code{"dic"}, and \code{"waic"}. The default value is none (\code{NULL}).
#' @param n_chains The number of chains to use with the simulation. The default value is 1.
#' @param method A character string of which Bayesian software to use. The options are \code{"jags"} and \code{"stan"}. The default is \code{"jags"}.
#' @param inits Initial values
#' @param c c
#' @param r r
#' @param beta_mu beta_mu
#' @param beta_tau beta_tau
#' @return A list of lists. Contains \code{model_code}, \code{model_data}, \code{model_inits}, and \code{model_params}.
#' @examples
#' #----------------------------------------------------------------------------
#' # model_info example
#' #----------------------------------------------------------------------------
#' model_info <- model_info(Surv(start, stop, event) ~ transplant, data = heart, id = "id")
#' @import dplyr
#' @importFrom coda as.mcmc
#' @export
model_info <- function(formula, data = NULL, id = NULL, model_selection = NULL,
                       n_chains = 2, inits = NULL, method = "jags", c = 0.001,
                       r = 0.1, beta_mu = 0, beta_tau = 0.0001) {
  #-----------------------------------------------------------------------------
  # Argument checks
  #-----------------------------------------------------------------------------
  # Need "deviance" otherwise the deviance chain will not be returned.
  if ("dic" %in% model_selection && method == "jags") {
    model_selection <- c("dic", "deviance")
  }

  #-----------------------------------------------------------------------------
  # model_data
  #-----------------------------------------------------------------------------
  # Store survival type and create dataframe of survival data
  surv_type <- surv_type(formula, data) # See R/utils.R
  data <- surv_data(formula, data, id) # See R/surv-data.R

  # Check for random effect terms
  frailty_names <- colnames(data[grepl("frailty.", names(data), fixed = TRUE)])

  # Store the covariate names.
  if (surv_type == "right") {
    cov_names <- colnames(data)[4:ncol(data)]
    # Remove random effect names
    cov_names <- cov_names[! cov_names %in% frailty_names]
  }
  if (surv_type == "counting") {
    cov_names <- colnames(data)[5:ncol(data)]
    # Remove random effect names
    cov_names <- cov_names[! cov_names %in% frailty_names]
  }

  # Number of subjects.
  n_subjects <- data %>% group_by(id) %>% n_groups()

  # Unique event == 1 times.
  time_unique_events <- data %>% filter(event == 1) %>% distinct(stop) %>% arrange(stop) %>% .$stop
  n_unique_events <- length(time_unique_events)

  # Unique event == 1 times and maximum censoring time.
  time_max_censor <- data %>% filter(event == 0) %>% .$stop %>% max()
  if (time_max_censor >= max(time_unique_events)) {
    time_unique_events_plus <- c(time_unique_events, time_max_censor + 0.01)
  } else {
    time_unique_events_plus <- c(time_unique_events, max(time_unique_events) + 0.01)
  }

  # is_at_risk is an observed process taking the value 1 or 0 according to
  # whether or not subject i is observed at time t. It answers the question,
  # "is subject i still 'at risk' during event == 1 time j?"
  # Suppose N_i(t) counts the number of event = 1 up to time t. If subject i is
  # observed to fail during [t, t+dt), counting_proc = 1 else counting_proc = 0.
  ## Final stop time for each subject (time when event = 1 OR event = 0).
  time_final <- data %>% group_by(id) %>% top_n(n = 1, wt = stop) %>% .$stop
  ## Final event value for each subject.
  event_final <- data %>% group_by(id) %>% top_n(n = 1, wt = stop) %>% .$event
  is_at_risk <- matrix(NA_integer_, nrow = n_subjects, ncol = n_unique_events)
  counting_proc <- matrix(NA_integer_, nrow = n_subjects, ncol = n_unique_events)
  for (i in 1:n_subjects) { # number of subjects
    for (j in 1:n_unique_events) { # number of unique event == 1 times.
      if (time_final[i] >= time_unique_events_plus[j]) {
        is_at_risk[i,j] <- 1
      } else {
        is_at_risk[i,j] <- 0
      }
      if (time_unique_events_plus[j+1] > time_final[i]) {
        counting_proc[i,j] <- is_at_risk[i,j] * event_final[i]
      } else {
        counting_proc[i,j] <- 0
      }
    }
  }

  # Create covariate matrices. If time-varying data, create a matrix with rows
  # for each unique subject and columns for each unique event == 1 time.
  # If non-time-varying, just assign the covariate.
  if(surv_type == "counting") {
    # add a serial number for each id
    data <- data %>% group_by(id) %>% mutate(id_serial = 1:length(start))
    # create a "long" format data.frame of subjects, observation number, and start/stop times
    data_long <- data.frame(
      id = rep(1:n_subjects, each = n_unique_events * max(data$id_serial)),
      id_serial = rep(1:max(data$id_serial), max(data$id_serial), n_subjects * max(data$id_serial)),
      time_unique_events = rep(time_unique_events, each = max(data$id_serial))
    )
    # merge in covariate values into long format
    data_long <- left_join(data_long,
                           data[, c("id", "id_serial", "start", "stop", cov_names)],
                           by = c("id", "id_serial")
    )
    # eliminate missings (for which no 2nd observation took place)
    data_long <- na.omit(data_long)
    # Get covariate values
    data_long <- data_long %>% mutate_each_(funs(. * ((start < time_unique_events) & (stop >= time_unique_events))), cov_names)
    # sum on the ids by fail time
    data_long <- data_long %>% group_by(id, time_unique_events) %>% summarise_each_(funs(sum(.)), cov_names)
    # convert to a matrix
    for ( i in 1:length(cov_names)) {
      assign(cov_names[i],
             matrix(data_long[[cov_names[i]]],
                    ncol = n_unique_events,
                    byrow = TRUE,
                    dimnames = list(1:n_subjects, time_unique_events)
             )
      )
    }
  }
  if(surv_type == "right") {
    for (i in 1:length(cov_names)) {
      assign(cov_names[i], data[[cov_names[i]]])
    }
  }

  # Random effect covariates
  if(length(frailty_names) > 0) {
    for (i in 1:length(frailty_names)) {
      assign(
        frailty_names[i],
        data %>% distinct(id) %>% .[[frailty_names]]
      )
      n_distinct_frailty <- length(unique(mget(frailty_names[i])[[1]]))
    }
  } else {
    frailty_names <- NULL
  }

  # Create data list.
  model_data <- c(
    list(
      n_subjects = n_subjects,
      n_unique_events = n_unique_events,
      time_unique_events_plus = time_unique_events_plus,
      is_at_risk = is_at_risk,
      counting_proc = counting_proc,
      c = c,
      r = r,
      beta_mu = beta_mu,
      beta_tau = beta_tau
    ),
    mget(c(cov_names, frailty_names))
  )

  if (!is.null(frailty_names)) {
    model_data <- c(model_data, list(n_distinct_frailty = n_distinct_frailty))
  }

  #-----------------------------------------------------------------------------
  # model_params
  #-----------------------------------------------------------------------------
  beta_names <- paste("beta_", cov_names, sep = "")
  model_params <- c(beta_names, model_selection)

  if(!is.null(frailty_names)) {
    model_params <- c(model_params, "sigma")
  }

  # runjags mutate function for converting to exp()
  mutate_to_exp <- function(x, vars) {
    x <- as.mcmc(as.matrix(x))
    newmcmc <- x[, vars, drop = FALSE]
    newmcmc[] <- exp(newmcmc[])
    dimnames(newmcmc) <- list(dimnames(x)[[1]], paste("exp(", vars, ")", sep = ""))
    return(as.mcmc(newmcmc))
  }
  # Argument value to pass to run.jags
  mutated <- list(mutate_to_exp, beta_names)

  #-----------------------------------------------------------------------------
  # model_inits
  #-----------------------------------------------------------------------------
  # Create initial value list.
  model_inits <- function(inits, method, n_chains, beta_names, n_unique_events,
                          frailty_names) {
    if (is.null(inits)) {
      if (method == "jags") {
        jags_inits <- function(n_chains, beta_names, n_unique_events,
                               frailty_names) {
          beta_inits <- rnorm(length(beta_names), 0, 3)
          hazard_proc_inits <- rep(1, n_unique_events)
          jags_inits <- c(
            list(hazard_proc_inits = hazard_proc_inits),
            beta_inits,
            .RNG.name = switch(
              n_chains,
              "1" = "base::Wichmann-Hill",
              "2" = "base::Marsaglia-Multicarry",
              "3" = "base::Super-Duper",
              "4" = "base::Mersenne-Twister"
            )
          )
          # Make sure hazard_proc_inits and beta_inits have correct names.
          names(jags_inits)[1] <- "hazard_proc"
          names(jags_inits)[2:(1+length(beta_names))] <- beta_names
          if(!is.null(frailty_names)) {
            jags_inits <- c(jags_inits, frailty_names = 1)
            names(jags_inits)[length(jags_inits)] <- "tau"
          }
          jags_inits
        }
        inits = lapply(
          1:n_chains,
          jags_inits,
          beta_names = beta_names,
          n_unique_events = n_unique_events,
          frailty_names = frailty_names
        )
      }

      if (method == "stan") {
        inits = "random"
      }
    }

    # Return
    inits
  }

  model_inits <- model_inits(
    inits = inits,
    method = method,
    n_chains = n_chains,
    beta_names = beta_names,
    n_unique_events = n_unique_events,
    frailty_names = frailty_names
  )

  #-----------------------------------------------------------------------------
  # model_code
  #-----------------------------------------------------------------------------
  # Create model sum
  if(surv_type == "counting") {
    index <- "[i,j]"
  } else {
    index <- "[i]"
  }
  model_sum <- paste(beta_names, " * ", cov_names, index, sep = "", collapse = " + ")
  # Random Effects
  if (!is.null(frailty_names)) {
    model_sum <- paste(model_sum, " + ", "b[", frailty_names, "[i]]", sep = "")
  }

  # Create model code
  model_code <- function(method, model_sum, beta_names, cov_names, surv_type, frailty_names) {
    # jags
    if(method == "jags") {
      model_text <- paste(
"model {
  for(j in 1:n_unique_events) {
    for(i in 1:n_subjects) {
      counting_proc[i,j] ~ dpois(intensity_proc[i,j]) # Likelihood
      intensity_proc[i,j] <- is_at_risk[i,j] *
                             exp(",
        paste(model_sum, sep = " "), ") *
                             hazard_proc[j] # Intensity
    }
    hazard_proc[j] ~ dgamma(mu[j], c)
    mu[j] <- hazard_proc_mean[j] * c # prior mean hazard
    hazard_proc_mean[j] <- r * (time_unique_events_plus[j+1] - time_unique_events_plus[j])
  }
", paste("  ", beta_names, " ~ dnorm(beta_mu, beta_tau) ", sep = "", collapse="\n"),
ifelse(length(frailty_names)>0, "
  for (k in 1:n_distinct_frailty) {
    b[k] ~ dnorm(0, tau)
  }
  tau ~ dgamma(0.001, 0.001)
  sigma <- sqrt(1/tau)",""),
"
}
", sep = "")
  }

    # Stan
    if(method == "stan") {

      if(surv_type == "counting") {
        index <- "[n_subjects, n_unique_events]; "
      } else {
        index <- "[n_subjects]; "
      }

      model_text <- paste(
"data {
  int<lower=0> n_subjects;
  int<lower=0> n_unique_events;
  real time_unique_events_plus[n_unique_events + 1];
  int<lower=0> is_at_risk[n_subjects, n_unique_events];
  int<lower=0> counting_proc[n_subjects, n_unique_events];
", paste("  real ", cov_names, index, sep = "", collapse = "\n"), "
  real c;
  real r;
}
parameters {
", paste("  real ", beta_names, "; ", sep = "", collapse = "\n"), "
  real<lower=0> hazard_proc[n_unique_events];
}
model {
", paste("  ", beta_names, " ~ normal(0, 1000); ", sep = "", collapse = "\n"), "
  for(j in 1:n_unique_events) {
    hazard_proc[j] ~ gamma(r * (time_unique_events_plus[j + 1] - time_unique_events_plus[j]) * c, c);
    for(i in 1:n_subjects) {
      if (is_at_risk[i, j] != 0) {
        increment_log_prob(
          poisson_log(
            counting_proc[i, j],
            is_at_risk[i, j] *
            exp(", paste(model_sum, sep = " "), ") *
            hazard_proc[j]
          )
        );
      }
    }
  }
}
", sep = "")
  }

  # Return
  model_text
  }

  model_code <- model_code(
    method = method,
    model_sum = model_sum,
    beta_names = beta_names,
    cov_names = cov_names,
    surv_type = surv_type,
    frailty_names = frailty_names
  )

  #-----------------------------------------------------------------------------
  # Return model_info list
  #-----------------------------------------------------------------------------
  list(
    model_code = model_code,
    model_data = model_data,
    model_inits = model_inits,
    model_params = model_params,
    mutated = mutated
  )
}

#-----------------------------------------------------------------------------
# R CMD check produces NOTES for variables assigned by dplyr, data.table, etc.
# With each new R release, check without this code to see if still necessary.
# http://stackoverflow.com/a/12429344
# Note: I think becase "stop" is already a defined variable, R CMD check isn't
# finding it for this case... will leave alone for now
#-----------------------------------------------------------------------------
globalVariables(c("event", "."))
