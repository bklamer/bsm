#' Stanford Heart Transplant Data
#'
#' Survival of patients on the waiting list for the Stanford heart transplant program.
#' Identical to the \code{survival} package \code{heart} dataset.
#'
#' @format A data frame with 172 rows and 8 variables:
#' \itemize{
#'   \item id: subject id
#'   \item start: start time for the current interval
#'   \item stop: stop time for the current interval
#'   \item event: status of subject (event = 1 is death, event = 0 is alive or censor)
#'   \item age: subject age minus 48 years
#'   \item year: year of acceptance (in years after 1 Nov 1967)
#'   \item surgery: prior bypass surgery (surgery = 1 is yes, surgery = 0 is no)
#'   \item transplant: received transplant (transplant = 1 is yes, transplant = 0 is no)
#' }
#' @source J Crowley and M Hu (1977). "Covariance analysis of heart transplant survival data." \emph{Journal of the American Statistical Association}, \bold{72}, 27--36.
"heart"

#' Gehan-Freirich Leukemia Data
#'
#' Time of remission to relapse for leukemia patients. The data come from a
#' matched-pairs design, where patients were paired according to remission
#' status (partial or complete) and then randomly assigned to the treatment or
#' control group (but most analyses have ignored this fact).
#'
#' @format A data frame with 42 rows and 4 variables:
#' \itemize{
#'   \item pair: indicator for subject pair
#'   \item time: weeks in remission
#'   \item event: status of subject (event = 1 is relapse, event = 0 is censor)
#'   \item treatment: treatment group (treatment = "6-MP" is drug, treatment = "control" is placebo)
#' }
#' @source Andersen, P. K. and Borgan, O. and Gill, R. D. and Keiding, N. (2012). \emph{Statistical Models Based on Counting Processes}. Springer-Verlag, New York.
"leukemia"

#' Sage-Grouse Nest Data
#'
#' Time to failure of Sage-Grouse nests located in Wyoming. Nests were
#' considered succesful and censored if at least one chick hatched within
#' (approximately) 28 days. All covariates are time-fixed.
#'
#' @format A data frame with 83 rows and 6 variables:
#' \itemize{
#'   \item id: nest identifier
#'   \item time: time in days
#'   \item event: status of nest (event = 1 is nest failure, event = 0 is censor)
#'   \item moisture: mean topographic wetness index (high values = increased soil moisture)
#'   \item shrub_height: variability (standard deviation) in shrub height
#'   \item sagebrush: percentage of Wyoming sagebrush cover
#' }
#' @source Kirol, Christopher P. and Beck, Jeffrey L. and Huzurbazar, Snehalata V. and Holloran, Matthew J. and Miller, Scott N. (2015). "Identifying Greater Sage-Grouse source and sink habitats for conservation planning in an energy development landscape." \emph{Ecological Applications}, \bold{25}(4), 668-990.
"nest"

#' Sage-Grouse Brood Data
#'
#' Time to failure of Sage-Grouse broods located in Wyoming. Broods were
#' considered succesful and censored if at least one chick lived past
#' (approximately) 36 days post-hatch. Covariates are time-dependent and
#' already in right-censored form.
#'
#' @format A data frame with 227 rows and 7 variables:
#' \itemize{
#'   \item id: brood identifier
#'   \item start: start time (days) for the current interval
#'   \item stop: stop time (days) for the current interval
#'   \item event: status of brood (event = 1 is brood failure, event = 0 is right-censored)
#'   \item disturbance: Surface disturbance cell count
#'   \item shrub_height: variability (standard deviation) in shrub height
#'   \item herb_cover: percentage of herbaceous cover
#' }
#' @source Kirol, Christopher P. and Beck, Jeffrey L. and Huzurbazar, Snehalata V. and Holloran, Matthew J. and Miller, Scott N. (2015). "Identifying Greater Sage-Grouse source and sink habitats for conservation planning in an energy development landscape." \emph{Ecological Applications}, \bold{25}(4), 668-990.
"nest"

#' Sage-Grouse Female Data
#'
#' Time to death of female Sage-Grouse located in Wyoming. Females were
#' considered succesful and censored if they survived the (approximately) 110
#' days corresponding from May to August. Covariates are time-dependent and
#' already in right-censored form.
#'
#' @format A data frame with 1002 rows and 7 variables:
#' \itemize{
#'   \item id: bird identifier
#'   \item start: start time (days) for the current interval
#'   \item stop: stop time (days) for the current interval
#'   \item event: status of bird (event = 1 is death, event = 0 is right-censored)
#'   \item roughness: mean topographic roughness index
#'   \item edge_distance: distance (km) to nearest anthropogenic edge
#'   \item shrub_height: variability (standard deviation) in shrub height
#' }
#' @source Kirol, Christopher P. and Beck, Jeffrey L. and Huzurbazar, Snehalata V. and Holloran, Matthew J. and Miller, Scott N. (2015). "Identifying Greater Sage-Grouse source and sink habitats for conservation planning in an energy development landscape." \emph{Ecological Applications}, \bold{25}(4), 668-990.
"female"
