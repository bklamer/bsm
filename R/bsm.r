#' Fit Baysian Cox-type models.
#'
#' Fit a Bayesian Cox-type regression model.
#'
#' @param formula A formula object, with the response on the left of a \code{~} operator, and the covariate terms on the right. The response must be a survival object as returned by the \code{Surv} function from the survival package.
#' @param data A \code{data.frame} in which to interpret the variables named in the formula and \code{id} argument.
#' @param id A character string of the subject id's column name in the \code{data.frame} (if the \code{data} argument is provided) or a vector of subject id's (if the \code{data} argument is not provided). The \code{id} argument is only needed for time-varying covariate survival data.
#' @param model_selection A character vector of information criteria variables. The reserved variable names for information criteria are \code{"deviance"}, \code{"dic"}, and \code{"waic"}. The default value is none (\code{NULL}).
#' @param n_chains The number of chains to use with the simulation. The default value is 1.
#' @param n_cores The number of cpu cores to use with the simulation. Should be equal to or less than \code{n_chains}. The default value is 1.
#' @param adapt The number of adaptive iterations to use at the start of the simulation. The default value is 1000.
#' @param warmup The number of warmup iterations after adaptation. The default value is 2000.
#' @param iter The number of iterations used for inference (Total iterations = adapt + warmup + iter). The default value is 4000. (Will automatically take \code{iter} \eqn{\times} \code{thin} samples so there is no need for manual adjustment for thinning values.)
#' @param thin The thinning value. The default value is 1.
#' @param method A character string of which Bayesian software to use. The options are \code{"jags"} and \code{"stan"}. The default is \code{"jags"}.
#' @param inits Initial values
#' @param c c
#' @param r r
#' @param beta_mu beta_mu
#' @param beta_tau beta_tau
#' @return If \code{method = "jags"}, a \code{runjags} object as returned by the function \code{run.jags} from the runjags package. If \code{method = "stan"}, a \code{stanfit} object as returned by the function \code{stan} from the rstan package.
#' @examples
#' \dontrun{
#' #----------------------------------------------------------------------------
#' # Time-varying covariates
#' #----------------------------------------------------------------------------
#' mod1 <- bsm(Surv(start, stop, event) ~ transplant, data = heart, id = "id")
#' summary(mod1)
#' mod2 <- bsm(Surv(start, stop, event) ~ transplant * (age + surgery), data = heart, id = "id")
#' summary(mod2)
#'
#' #----------------------------------------------------------------------------
#' # Non-time-varying covariate
#' #----------------------------------------------------------------------------
#' mod3 <- bsm(Surv(time, event) ~ treatment, data = leukemia)
#' summary(mod3)
#' }
#' @import dplyr Rcpp
#' @importFrom runjags run.jags
#' @importFrom rstan stan
#' @export
bsm <- function(formula, data = NULL, id = NULL, model_selection = NULL,
                n_chains = 2, n_cores = 1, warmup = 2000, adapt = 1000,
                iter = 4000, thin = 1, method = "jags", inits = NULL,
                c = 0.001, r = 0.1, beta_mu = 0, beta_tau = 0.0001) {
  #-----------------------------------------------------------------------------
  # Create model information
  #-----------------------------------------------------------------------------
  model_info <- model_info(
    formula = formula,
    data = data,
    id = id,
    model_selection = model_selection,
    n_chains = n_chains,
    inits = inits,
    method = method,
    c = c,
    r = r,
    beta_mu = beta_mu,
    beta_tau = beta_tau
  )
  #-----------------------------------------------------------------------------
  # Prepare for model fitting
  #-----------------------------------------------------------------------------
  # jags checks
  if (method == "jags") {
    # interpret n_cores
    if (n_cores > 1) {
      jags_method = "parallel"
    } else {
      jags_method = "interruptible"
    }
    # Choose appropriate processing method if checking information criteria.
    if (!is.null(model_selection) && n_cores > 1) {
      warning("Check the \"model_selection\" argument and/or the \"n_cores\" argument. You cannot use parallel processing AND check information criteria. The single process method was used instead of the parallel method.")
      jags_method = "interruptible"
    }
    # Check number of chains. Jags init value func only supports up to 4 chains.
    if (n_chains > 4) {
      stop("Check the n_chains argument. Please choose 4 chains or less.")
    }
  }

  #-----------------------------------------------------------------------------
  # Fit Bayesian model
  #-----------------------------------------------------------------------------
  fitter <- function(
    model_code,
    model_params,
    mutated,
    model_data,
    n_chains,
    n_cores,
    model_inits,
    warmup,
    adapt,
    iter,
    thin,
    method,
    model_selection,
    jags_method
  ) {
    # jags
    if (method == "jags") {
      fit <- run.jags(
        model = model_code,
        monitor = model_params,
        data = model_data,
        n.chains = n_chains,
        inits = model_inits,
        burnin = warmup,
        adapt = adapt,
        sample = iter,
        mutate = mutated,
        thin = thin,
        method = jags_method
      )
      # Fix issues with DIC calculation and Fix issues with how they are returned
      # depending on method used.
      if ("dic" %in% model_selection) {
        # Calculate pD and dic
        fit.sum_mean_deviance <- sum(fit$deviance.table[,1])
        fit.sum_mean_pd <- sum(fit$deviance.table[,2], na.rm = TRUE)
        fit.sum_mean_popt <- sum(fit$deviance.table[,3], na.rm = TRUE)
        fit.dic <- fit.sum_mean_deviance + fit.sum_mean_pd
        fit.ped <- fit.sum_mean_deviance + fit.sum_mean_popt

        # Put them back into runjags object
        fit[["pd"]] <- fit.sum_mean_pd
        fit[["deviance.sum"]][["sum.mean.pD"]] <- fit.sum_mean_pd
        fit[["deviance.sum"]][["sum.mean.pOpt"]] <- fit.sum_mean_popt
        fit[["dic"]][["dic"]] <- fit.dic
        fit[["dic"]][["ped"]] <- fit.ped
        fit[["dic"]][["meanpd"]] <- fit.sum_mean_pd
        fit[["dic"]][["meanpopt"]] <- fit.sum_mean_popt
      }
    }

    # Stan
    if (method == "stan") {
      fit <- stan(
        model_code = model_code,
        data = model_data,
        chains = n_chains,
        warmup = warmup,
        iter = iter + warmup,
        cores = n_cores
      )
    }

    # Return
    fit
  }

  fit <- fitter(
    model_code = model_info[["model_code"]],
    model_params = model_info[["model_params"]],
    mutate = model_info[["mutated"]],
    model_data = model_info[["model_data"]],
    n_chains = n_chains,
    n_cores = n_cores,
    model_inits = model_info[["model_inits"]],
    warmup = warmup,
    adapt = adapt,
    iter = iter,
    thin = thin,
    method = method,
    model_selection = model_selection,
    jags_method = jags_method
  ) # See R/bsm-utils.r

  #-----------------------------------------------------------------------------
  # Return results
  #-----------------------------------------------------------------------------
  fit
}
