#' Create a dataframe based on a survival formula.
#'
#' This function creates a dataframe containing the necessary data to fit a survival model.
#'
#' @param formula A formula object, with the response on the left of a \code{\~} operator, and the covariate terms on the right. The response must be a survival object as returned by the \code{Surv} function.
#' @param data A \code{data.frame} in which to interpret the variables named in the formula and \code{id} argument.
#' @param id A character string of the subject id's column name in the \code{data.frame} (if the \code{data} argument is provided) or a vector of subject id's (if the \code{data} argument is not provided). The \code{id} argument is only needed for time-varying covariate survival data.
#' @return A \code{data.frame} of survival data.
#' @examples
#' #----------------------------------------------------------------------------
#' # Time-varying covariates
#' #----------------------------------------------------------------------------
#' data1 <- surv_data(Surv(start, stop, event) ~ transplant, data = heart, id = "id")
#' head(data1)
#' data2 <- surv_data(Surv(start, stop, event) ~ transplant * (age + surgery), data = heart, id = "id")
#' head(data2)
#' #----------------------------------------------------------------------------
#' # Non-time-varying covariate
#' #----------------------------------------------------------------------------
#' data3 <- surv_data(Surv(time, event) ~ treatment, data = leukemia)
#' head(data3)
#' @import survival
#' @export
surv_data <- function(formula, data = NULL, id = NULL) {
  # Some parts based on the code in "coxph.R" from the survival package.
  #-----------------------------------------------------------------------------
  # Check the arguments and set up what the arguments bring in.
  # Be careful changing the order of these statements.
  #-----------------------------------------------------------------------------
  if (missing(formula)) {
    stop("Check the formula argument. The formula seems to be missing.")
  }

  mf <- model.frame(formula, data)
  if (nrow(mf) == 0) {
    stop("Check the data argument. There are no (non-missing) observations.")
  }
  if (!is_surv(formula, data)) { # See R/utils.R
    stop("Check the formula argument. The formula response must be a Surv() object.")
  }

  surv_type <- surv_type(formula, data) # See R/utils.R
  if (surv_type != "right" && surv_type != "counting") {
    stop(paste("Check the formula argument. The bsm package doesn't support \"", surv_type, "\" survival data.", sep=''))
  }

  if (surv_type == "counting") {
    if (is.null(id)) {
      stop("Check the id argument. You either forgot the id argument or it doesn't contain any data. The id argument is required for time-varying covariates.")
    }

    if (is.null(data)) {
      if (length(id) != nrow(mf)) {
        stop("Check the id argument. You didn't supply the data argument so the vector length of the id argument should match the vector lengths of the Surv() data.")
      }
      id_data <- id
    }

    if (!is.null(data)) {
      if (!is.character(id)) {
        stop("Check the id argument. The id argument should be a character string.")
      }
      if (!(id %in% colnames(data))) {
        stop("Check the id argument. The id argument is not found in this data.frame.")
      }
      id_data <- data[[id]]
    }
  }

  if (surv_type == "right") {
      id_data <- c(1:nrow(mf))
  }

  #-----------------------------------------------------------------------------
  # Create matrix of id data.
  #-----------------------------------------------------------------------------
  id_data <- as.matrix(as.integer(as.factor(id_data)))
  colnames(id_data) <- "id"

  #-----------------------------------------------------------------------------
  # Create matrix of Surv data.
  #-----------------------------------------------------------------------------
  Y <- model.response(mf)
  # cbind is needed to turn class "Surv" to class "matrix"
  Y <- cbind(Y)
  # Create consistent column names
  if (surv_type == "counting") {
    colnames(Y) <- c("start", "stop", "event")
  }
  if (surv_type == "right") {
    colnames(Y) <- c("stop", "event")
  }

  #-----------------------------------------------------------------------------
  # Create matrix of covariate data.
  #-----------------------------------------------------------------------------
  Terms <- terms(mf)
  X <- model.matrix(Terms, mf)
  # Can't have "." in variable names for stan. model.matrix creates ":", but
  # data.frame will turn that into ".". This fixes the case when interactions
  # are in the formula.
  colnames(X) <- sub(":", "_x_", colnames(X), fixed = TRUE)

  # Drop the intercept from X
  attr(Terms, "intercept") <- 1
  adrop <- 0  # columns of "assign" to be dropped; 0= intercept
  Xatt <- attributes(X)
  xdrop <- Xatt$assign %in% adrop  #columns to drop (always the intercept)
  X <- X[, !xdrop, drop=FALSE]

  #-----------------------------------------------------------------------------
  # Create dataframe of id, Surv, and covariate data.
  #-----------------------------------------------------------------------------
  df <- data.frame(id_data, Y, X)
  df <- df[order(df$id), ] # Make sure id stay ordered for fitting in bsm()
  df
}
