% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/data.r
\docType{data}
\name{nest}
\alias{nest}
\title{Sage-Grouse Nest Data}
\format{A data frame with 83 rows and 6 variables:
\itemize{
  \item id: nest identifier
  \item time: time in days
  \item event: status of nest (event = 1 is nest failure, event = 0 is censor)
  \item moisture: mean topographic wetness index (high values = increased soil moisture)
  \item shrub_height: variability (standard deviation) in shrub height
  \item sagebrush: percentage of Wyoming sagebrush cover
}}
\source{
Kirol, Christopher P. and Beck, Jeffrey L. and Huzurbazar, Snehalata V. and Holloran, Matthew J. and Miller, Scott N. (2015). "Identifying Greater Sage-Grouse source and sink habitats for conservation planning in an energy development landscape." \emph{Ecological Applications}, \bold{25}(4), 668-990.

Kirol, Christopher P. and Beck, Jeffrey L. and Huzurbazar, Snehalata V. and Holloran, Matthew J. and Miller, Scott N. (2015). "Identifying Greater Sage-Grouse source and sink habitats for conservation planning in an energy development landscape." \emph{Ecological Applications}, \bold{25}(4), 668-990.
}
\usage{
nest

nest
}
\description{
Time to failure of Sage-Grouse nests located in Wyoming. Nests were
considered succesful and censored if at least one chick hatched within
(approximately) 28 days. All covariates are time-fixed.

Time to failure of Sage-Grouse broods located in Wyoming. Broods were
considered succesful and censored if at least one chick lived past
(approximately) 36 days post-hatch. Covariates are time-dependent and
already in right-censored form.
}
\keyword{datasets}

