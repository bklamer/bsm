context("bsm")

test_that("The Survival type is supported", {
  df <- data.frame(
    start = 1,
    stop = 1,
    event = 1,
    x = 1
  )
  expect_error(
    bsm(Surv(start, stop, event, type = "interval") ~ x, data = df),
    "Check the formula argument. The bsm package doesn't support \"interval\" survival data.",
    fixed = TRUE
  )
})

test_that("The number of cores is correct for information criteria", {
  df <- data.frame(
    stop = 1,
    event = 1,
    x = 1
  )
  expect_warning(
    bsm(
      Surv(stop, event) ~ x,
      data = df,
      info_criteria = "dic",
      n_cores = 2
    ),
    "Check the \"info_criteria\" argument and/or the \"n_cores\" argument. You cannot use parallel processing AND check information criteria. The single process method was used instead of the parallel method.",
    fixed = TRUE
  )
})

test_that("The number of chains is 4 or less", {
  df <- data.frame(
    stop = 1,
    event = 1,
    x = 1
  )
  expect_error(
    bsm(
      Surv(stop, event) ~ x,
      data = df,
      n_chains = 5
    ),
    "Check the n_chains argument. Please choose 4 chains or less.",
    fixed = TRUE
  )
})
