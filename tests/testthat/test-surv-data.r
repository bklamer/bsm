context("surv_data")

test_that("The formula is not missing", {
  expect_error(
    surv_data(data = NULL),
    "Check the formula argument. The formula seems to be missing.",
    fixed = TRUE
  )
})

test_that("No (non-missing) data in dataframe", {
  df <- data.frame(
    stop = 1,
    event = 1,
    x = NA
  )
  expect_error(
    surv_data(Surv(stop, event) ~ x, data = df),
    "Check the data argument. There are no (non-missing) observations.",
    fixed = TRUE
  )
})

test_that("The formula must be a Surv object", {
  df <- data.frame(
    y = 1,
    x = 1
  )
  expect_error(
    surv_data(y ~ x, data = df),
    "Check the formula argument. The formula response must be a Surv() object.",
    fixed = TRUE
  )
})

test_that("The Survival type is supported", {
  df <- data.frame(
    start = 1,
    stop = 1,
    event = 1,
    x = 1
  )
  expect_error(
    surv_data(Surv(start, stop, event, type = "interval") ~ x, data = df),
    "Check the formula argument. The bsm package doesn't support \"interval\" survival data.",
    fixed = TRUE
  )
})

test_that("The id argument is not missing", {
  df <- data.frame(
    start = 0,
    stop = 1,
    event = 1,
    x = 1
  )
  expect_error(
    surv_data(Surv(start, stop, event) ~ x, data = df),
    "Check the id argument. You either forgot the id argument or it doesn't contain any data. The id argument is required for time-varying covariates.",
    fixed = TRUE
  )
})

test_that("The id argument should be equal length vector", {
  df <- data.frame(
    start = c(0, 0),
    stop = c(1, 1),
    event = c(0, 1),
    x = c(1, 1)
  )
  id <- c(1)
  expect_error(
    surv_data(Surv(df$start, df$stop, df$event) ~ df$x, id = id),
    "Check the id argument. You didn't supply the data argument so the vector length of the id argument should match the vector lengths of the Surv() data.",
    fixed = TRUE
  )
})

test_that("The id argument should be a character", {
  df <- data.frame(
    id = 1,
    start = 0,
    stop = 1,
    event = 1,
    x = 1
  )
  expect_error(
    surv_data(Surv(start, stop, event) ~ x, data = df, id = df$id),
    "Check the id argument. The id argument should be a character string.",
    fixed = TRUE
  )
})

test_that("The id argument is found in dataframe", {
  df <- data.frame(
    ID = 1,
    start = 0,
    stop = 1,
    event = 1,
    x = 1
  )
  expect_error(
    surv_data(Surv(start, stop, event) ~ x, data = df, id = "id"),
    "Check the id argument. The id argument is not found in this data.frame.",
    fixed = TRUE
  )
})

test_that("Factor id values should be correctly ordered integers", {
  df <- data.frame(
    id = c("B", "B", "A", "A", 3, 3),
    check_id = c(1, 1, 2, 2, 3, 3),
    start = c(0, 1, 0, 1, 0, 1),
    stop = c(1, 2, 1, 2, 1, 2),
    event = c(0, 1, 0, 0, 0, 1),
    x = c(1, 2, 1, 1, 1, 2)
  )
  surv_df <- surv_data(Surv(start, stop, event) ~ x + check_id, data = df, id = "id")
  expect_equal(surv_df$id, c(1, 1, 2, 2, 3, 3))
  expect_equal(surv_df$check_id, c(3, 3, 2, 2, 1, 1))
})

test_that("Simple column names are OK", {
  # Base test. Nothing changed. For "counting" survival type.
  df1 <- data.frame(
    id = c("B", "B", "A", "A", 3, 3),
    start = c(0, 1, 0, 1, 0, 1),
    stop = c(1, 2, 1, 2, 1, 2),
    event = c(0, 1, 0, 0, 0, 1),
    x1 = c(1, 2, 1, 1, 1, 2),
    x2 = c(1, 2, 1, 1, 1, 2)
  )
  surv_df1 <- surv_data(Surv(start, stop, event) ~ x1 + x2, data = df1, id = "id")
  expect_equal(
    names(surv_df1),
    names(df1)
  )

  # surv_data of df2 should match names of df1
  df2 <- data.frame(
    ID = c("B", "B", "A", "A", 3, 3),
    START.time = c(0, 1, 0, 1, 0, 1),
    STOP.time = c(1, 2, 1, 2, 1, 2),
    STATUS = c(0, 1, 0, 0, 0, 1),
    x1 = c(1, 2, 1, 1, 1, 2),
    x2 = c(1, 2, 1, 1, 1, 2)
  )
  surv_df2 <- surv_data(Surv(START.time, STOP.time, STATUS) ~ x1 + x2, data = df2, id = "ID")
  expect_equal(
    names(surv_df2),
    names(df1)
  )

  # Base test. Nothing changed. For "right" survival type.
  df3 <- data.frame(
    id = c(1, 2, 3, 4, 5, 6),
    stop = c(1, 2, 1, 2, 1, 2),
    event = c(0, 1, 0, 0, 0, 1),
    x1 = c(1, 2, 1, 1, 1, 2),
    x2 = c(1, 2, 1, 1, 1, 2)
  )
  surv_df3 <- surv_data(Surv(stop, event) ~ x1 + x2, data = df3)
  expect_equal(
    names(surv_df3),
    names(df3)
  )
})

test_that("Complex column names are OK", {
  df <- data.frame(
    id = c("B", "B", "A", "A", 3, 3),
    start = c(0, 1, 0, 1, 0, 1),
    stop = c(1, 2, 1, 2, 1, 2),
    event = c(0, 1, 0, 0, 0, 1),
    x1 = c(1, 2, 1, 1, 1, 2),
    x2 = c(1, 2, 1, 1, 1, 2)
  )
  surv_df <- surv_data(Surv(start, stop, event) ~ x1*x2, data = df, id = "id")
  expect_equal(
    names(surv_df)[7],
    "x1_x_x2"
  )
})

test_that("data.frame is returned", {
  df <- data.frame(
    id = c("B", "B", "A", "A", 3, 3),
    start = c(0, 1, 0, 1, 0, 1),
    stop = c(1, 2, 1, 2, 1, 2),
    event = c(0, 1, 0, 0, 0, 1),
    x1 = c(1, 2, 1, 1, 1, 2),
    x2 = c(1, 2, 1, 1, 1, 2)
  )
  surv_df <- surv_data(Surv(start, stop, event) ~ x1*x2, data = df, id = "id")
  expect_equal(
    class(surv_df),
    "data.frame"
  )
})
