# Manual Tests

The tests in this directory have long run times and should not be run during `R CMD check`.

To run these tests, use `testthat::test_dir(path = "tests/manual/")`
