context("bsm-long")

test_that("Model1 fits are good", {
  # Models
  ## jags
  bsm_jags_mod1 <- bsm(
    Surv(start, stop, event) ~ transplant,
    data = heart,
    id = "id",
    adapt = 400,
    warmup = 500,
    iter = 1000,
    method = "jags",
    n_cores = 2,
    n_chains = 2
  )
  ## stan
  bsm_stan_mod1 <- bsm(
    Surv(start, stop, event) ~ transplant,
    data = heart,
    id = "id",
    warmup = 500,
    iter = 1000,
    method = "stan",
    n_cores = 2,
    n_chains = 2
  )
  ## Frequentist
  coxph_mod1 <- coxph(Surv(start, stop, event) ~ transplant, data = heart)

  # Estimates
  coxph_mod1_mean <- coxph_mod1$coefficients[[1]]
  coxph_mod1_se <- sqrt(diag(coxph_mod1$var))
  bsm_jags_mod1_mean <- bsm_jags_mod1$summary$statistics[1]
  bsm_jags_mod1_se <- bsm_jags_mod1$summary$statistics[2]
  bsm_stan_mod1_mean <- mean(rstan::extract(bsm_stan_mod1)[[1]])
  bsm_stan_mod1_se <- sd(rstan::extract(bsm_stan_mod1)[[1]])

  # Mean comparisons
  ## jags
  expect_equal(
    bsm_jags_mod1_mean,
    coxph_mod1_mean,
    tolerance = 0.05,
    scale = 1
  )
  ## stan
  expect_equal(
    bsm_stan_mod1_mean,
    coxph_mod1_mean,
    tolerance = 0.05,
    scale = 1
  )

  # Standard deviation comparisons
  ## jags
  expect_equal(
    bsm_jags_mod1_se,
    coxph_mod1_se,
    tolerance = 0.05,
    scale = 1
  )
  # stan
  expect_equal(
    bsm_stan_mod1_se,
    coxph_mod1_se,
    tolerance = 0.05,
    scale = 1
  )
})

test_that("Model2 fits are good", {
  # Models
  ## jags
  bsm_jags_mod2 <- bsm(
    Surv(start, stop, event) ~ transplant * (age + surgery),
    data = heart,
    id = "id",
    adapt = 400,
    warmup = 1000,
    iter = 2000,
    method = "jags",
    n_cores = 2,
    n_chains = 2
  )
  ## stan
  bsm_stan_mod2 <- bsm(
    Surv(start, stop, event) ~ transplant * (age + surgery),
    data = heart,
    id = "id",
    warmup = 1000,
    iter = 2000,
    method = "stan",
    n_cores = 2,
    n_chains = 2
  )
  ## Frequentist
  coxph_mod2 <- coxph(
    Surv(start, stop, event) ~ transplant * (age + surgery),
    data = heart
  )

  # Estimates
  coxph_mod2_mean_transplant1 <- coxph_mod2$coefficients[[1]]
  coxph_mod2_mean_age <- coxph_mod2$coefficients[[2]]
  coxph_mod2_mean_surgery <- coxph_mod2$coefficients[[3]]
  coxph_mod2_mean_transplant1_x_age <- coxph_mod2$coefficients[[4]]
  coxph_mod2_mean_transplant1_x_surgery <- coxph_mod2$coefficients[[5]]

  coxph_mod2_se_transplant1 <- sqrt(diag(coxph_mod2$var))[1]
  coxph_mod2_se_age <- sqrt(diag(coxph_mod2$var))[2]
  coxph_mod2_se_surgery <- sqrt(diag(coxph_mod2$var))[3]
  coxph_mod2_se_transplant1_x_age <- sqrt(diag(coxph_mod2$var))[4]
  coxph_mod2_se_transplant1_x_surgery <- sqrt(diag(coxph_mod2$var))[5]

  bsm_jags_mod2_mean_beta_transplant1 <- bsm_jags_mod2$summary$statistics[1,1]
  bsm_jags_mod2_mean_beta_age <- bsm_jags_mod2$summary$statistics[2,1]
  bsm_jags_mod2_mean_beta_surgery <- bsm_jags_mod2$summary$statistics[3,1]
  bsm_jags_mod2_mean_beta_transplant1_x_age <- bsm_jags_mod2$summary$statistics[4,1]
  bsm_jags_mod2_mean_beta_transplant1_x_surgery <- bsm_jags_mod2$summary$statistics[5,1]

  bsm_jags_mod2_se_beta_transplant1 <- bsm_jags_mod2$summary$statistics[1,2]
  bsm_jags_mod2_se_beta_age <- bsm_jags_mod2$summary$statistics[2,2]
  bsm_jags_mod2_se_beta_surgery <- bsm_jags_mod2$summary$statistics[3,2]
  bsm_jags_mod2_se_beta_transplant1_x_age <- bsm_jags_mod2$summary$statistics[4,2]
  bsm_jags_mod2_se_beta_transplant1_x_surgery <- bsm_jags_mod2$summary$statistics[5,2]

  bsm_stan_mod2_mean_beta_transplant1 <- mean(rstan::extract(bsm_stan_mod2)[[1]])
  bsm_stan_mod2_mean_beta_age <- mean(rstan::extract(bsm_stan_mod2)[[2]])
  bsm_stan_mod2_mean_beta_surgery <- mean(rstan::extract(bsm_stan_mod2)[[3]])
  bsm_stan_mod2_mean_beta_transplant1_x_age <- mean(rstan::extract(bsm_stan_mod2)[[4]])
  bsm_stan_mod2_mean_beta_transplant1_x_surgery <- mean(rstan::extract(bsm_stan_mod2)[[5]])

  bsm_stan_mod2_se_beta_transplant1 <- sd(rstan::extract(bsm_stan_mod2)[[1]])
  bsm_stan_mod2_se_beta_age <- sd(rstan::extract(bsm_stan_mod2)[[2]])
  bsm_stan_mod2_se_beta_surgery <- sd(rstan::extract(bsm_stan_mod2)[[3]])
  bsm_stan_mod2_se_beta_transplant1_x_age <- sd(rstan::extract(bsm_stan_mod2)[[4]])
  bsm_stan_mod2_se_beta_transplant1_x_surgery <- sd(rstan::extract(bsm_stan_mod2)[[5]])

  coxph_mod2_mean_vec <- c(
    coxph_mod2_mean_transplant1,
    coxph_mod2_mean_age,
    coxph_mod2_mean_surgery,
    coxph_mod2_mean_transplant1_x_age,
    coxph_mod2_mean_transplant1_x_surgery
  )
  coxph_mod2_se_vec <- c(
    coxph_mod2_se_transplant1,
    coxph_mod2_se_age,
    coxph_mod2_se_surgery,
    coxph_mod2_se_transplant1_x_age,
    coxph_mod2_se_transplant1_x_surgery
  )

  bsm_jags_mod2_mean_vec <- c(
    bsm_jags_mod2_mean_beta_transplant1,
    bsm_jags_mod2_mean_beta_age,
    bsm_jags_mod2_mean_beta_surgery,
    bsm_jags_mod2_mean_beta_transplant1_x_age,
    bsm_jags_mod2_mean_beta_transplant1_x_surgery
  )
  bsm_jags_mod2_se_vec <- c(
    bsm_jags_mod2_se_beta_transplant1,
    bsm_jags_mod2_se_beta_age,
    bsm_jags_mod2_se_beta_surgery,
    bsm_jags_mod2_se_beta_transplant1_x_age,
    bsm_jags_mod2_se_beta_transplant1_x_surgery
  )

  bsm_stan_mod2_mean_vec <- c(
    bsm_stan_mod2_mean_beta_transplant1,
    bsm_stan_mod2_mean_beta_age,
    bsm_stan_mod2_mean_beta_surgery,
    bsm_stan_mod2_mean_beta_transplant1_x_age,
    bsm_stan_mod2_mean_beta_transplant1_x_surgery
  )
  bsm_stan_mod2_se_vec <- c(
    bsm_stan_mod2_se_beta_transplant1,
    bsm_stan_mod2_se_beta_age,
    bsm_stan_mod2_se_beta_surgery,
    bsm_stan_mod2_se_beta_transplant1_x_age,
    bsm_stan_mod2_se_beta_transplant1_x_surgery
  )

  # Mean comparisons
  ## jags
  expect_equal(
    bsm_jags_mod2_mean_vec,
    coxph_mod2_mean_vec,
    tolerance = 0.05,
    scale = 1
  )
  ## stan
  expect_equal(
    bsm_stan_mod2_mean_vec,
    coxph_mod2_mean_vec,
    tolerance = 0.07,
    scale = 1
  )

  # Standard deviation comparisons
  ## jags
  expect_equal(
    bsm_jags_mod2_se_vec,
    coxph_mod2_se_vec,
    tolerance = 0.05,
    scale = 1
  )
  # stan
  expect_equal(
    bsm_stan_mod2_se_vec,
    coxph_mod2_se_vec,
    tolerance = 0.05,
    scale = 1
  )
})

test_that("Model3 fits are good", {
  # Models
  ## jags
  bsm_jags_mod3 <- bsm(
    Surv(time, event) ~ treatment,
    data = leuk,
    adapt = 400,
    warmup = 500,
    iter = 1000,
    method = "jags",
    n_cores = 2,
    n_chains = 2
  )
  ## stan
  bsm_stan_mod3 <- bsm(
    Surv(time, event) ~ treatment,
    data = leuk,
    warmup = 500,
    iter = 1000,
    method = "stan",
    n_cores = 2,
    n_chains = 2
  )
  ## Frequentist
  coxph_mod3 <- coxph(Surv(time, event) ~ treatment, data = leuk)

  # Estimates
  coxph_mod3_mean <- coxph_mod3$coefficients[[1]]
  coxph_mod3_se <- sqrt(diag(coxph_mod3$var))
  bsm_jags_mod3_mean <- bsm_jags_mod3$summary$statistics[1]
  bsm_jags_mod3_se <- bsm_jags_mod3$summary$statistics[2]
  bsm_stan_mod3_mean <- mean(rstan::extract(bsm_stan_mod3)[[1]])
  bsm_stan_mod3_se <- sd(rstan::extract(bsm_stan_mod3)[[1]])

  # Mean comparisons
  ## jags
  expect_equal(
    bsm_jags_mod3_mean,
    coxph_mod3_mean,
    tolerance = 0.09,
    scale = 1
  )
  ## stan
  expect_equal(
    bsm_stan_mod3_mean,
    coxph_mod3_mean,
    tolerance = 0.09,
    scale = 1
  )

  # Standard deviation comparisons
  ## jags
  expect_equal(
    bsm_jags_mod3_se,
    coxph_mod3_se,
    tolerance = 0.08,
    scale = 1
  )
  # stan
  expect_equal(
    bsm_stan_mod3_se,
    coxph_mod3_se,
    tolerance = 0.08,
    scale = 1
  )
})
